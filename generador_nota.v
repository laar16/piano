`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:27:03 12/17/2017 
// Design Name: 
// Module Name:    generador_nota 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module generador_nota #(parameter limit=0)(
	input clk,
	output [3:0] sonido
	);
	
	reg [15:0] contar;
	reg [4:0] direccion;

	rom_sonido rs(direccion, sonido);

	always@(posedge clk) begin
		contar=contar+1;
		if(contar==limit) begin
			contar=0;
			direccion=direccion+1;
		end
	end
	

endmodule
