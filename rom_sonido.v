`timescale 1ns / 1ps

module rom_sonido(
   input [4:0] direccion,
	output [3:0] salida
	);

	reg [3:0] salida_reg;

	assign salida=salida_reg;

	always@(direccion) begin
		case(direccion)
			5'b00000: salida_reg=4'b1000;
			5'b00001: salida_reg=4'b1001;
			5'b00010: salida_reg=4'b1010;
			5'b00011: salida_reg=4'b1100;
			5'b00100: salida_reg=4'b1101;
			5'b00101: salida_reg=4'b1110;
			5'b00110: salida_reg=4'b1111;
			5'b00111: salida_reg=4'b1111;
			5'b01000: salida_reg=4'b1111;
			5'b01001: salida_reg=4'b1111;
			5'b01010: salida_reg=4'b1110;
			5'b01011: salida_reg=4'b1101;
			5'b01100: salida_reg=4'b1100;
			5'b01101: salida_reg=4'b1011;
			5'b01110: salida_reg=4'b1010;
			5'b01111: salida_reg=4'b1000;
			5'b10000: salida_reg=4'b0111;
			5'b10001: salida_reg=4'b0101;
			5'b10010: salida_reg=4'b0100;
			5'b10011: salida_reg=4'b0011;
			5'b10100: salida_reg=4'b0010;
			5'b10101: salida_reg=4'b0001;
			5'b10110: salida_reg=4'b0000;
			5'b10111: salida_reg=4'b0000;
			5'b11000: salida_reg=4'b0000;
			5'b11001: salida_reg=4'b0000;
			5'b11010: salida_reg=4'b0001;
			5'b11011: salida_reg=4'b0010;
			5'b11100: salida_reg=4'b0011;
			5'b11101: salida_reg=4'b0101;
			5'b11110: salida_reg=4'b0110;
			5'b11111: salida_reg=4'b1000;
		endcase
	end

endmodule
