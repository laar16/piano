`timescale 1ns / 1ps

module Piano(
 	input [5:0] teclas,
	input clk,
	input L,
	input R,
	output [3:0] sonido,
	output [1:0] LR
	);

	wire [5:0] sonido1, sonido2, sonido3, sonido4, sonido5, sonido6;
	reg [3:0] reg_sonido;

	assign sonido=reg_sonido;
	assign LR = {L,R};

	generador_nota #(12'd2986) gen1(clk, sonido1); //DO
	generador_nota #(12'd2660) gen2(clk, sonido2); //RE
	generador_nota #(12'd2370) gen3(clk, sonido3); //MI
	generador_nota #(11'd1993) gen4(clk, sonido4); //SO
	generador_nota #(11'd1775) gen5(clk, sonido5); //LA
	generador_nota #(11'd1660) gen6(clk, sonido6); //SI

	always @(teclas) begin
		case(teclas)
			6'b111110: reg_sonido=sonido1;
			6'b111101: reg_sonido=sonido2;
			6'b111011: reg_sonido=sonido3;
			6'b110111: reg_sonido=sonido4;
			6'b101111: reg_sonido=sonido5;
			6'b011111: reg_sonido=sonido6;
			default: reg_sonido=4'b0000;
		endcase
	end
endmodule
